/**
 * @file root sagas
 */

import { all } from 'redux-saga/effects';

// Place for sagas' app
import { sagas as LoginSaga } from '../modules/Login';
import { sagas as ShopSaga } from '../modules/Shop';
import { sagas as UserSaga } from '../modules/User';
import { sagas as RegisterSaga } from '../modules/Register';
import { sagas as MainLayout } from '../layouts/MainLayout';

/*----Sagas List-----------------*/
export default function* rootSaga() {
	yield all([RegisterSaga(), LoginSaga(), UserSaga(), ShopSaga(), MainLayout()]);
}
