import { initialState as LoginInitialState, name as LoginPage } from '../../modules/Login';
import { initialState as ShopInitialState, name as ShopPage } from '../../modules/Shop';
import { initialState as UserInitialState, name as UserPage } from '../../modules/User';
import { initialState as RegisterInitialState, name as RegisterPage } from '../../modules/Register';
import {
	initialState as MainLayoutInitialState,
	name as MainLayout,
} from '../../layouts/MainLayout';
import IStore from './IStore';

export const initialState: IStore = {
	[LoginPage]: LoginInitialState,
	[UserPage]: UserInitialState,
	[ShopPage]: ShopInitialState,
	[RegisterPage]: RegisterInitialState,
	[MainLayout]: MainLayoutInitialState,
	router: null,
};

export default initialState;
