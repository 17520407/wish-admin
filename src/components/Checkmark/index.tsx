import * as React from 'react';
interface IProps {
	message: string;
}

export const CheckMark: React.FC<IProps> = ({ message }: IProps) => (
	<React.Fragment>
		<div className="success-checkmark">
			<div className="check-icon">
				<span className="icon-line line-tip"/>
				<span className="icon-line line-long"/>
				<div className="icon-circle"/>
				<div className="icon-fix"/>
			</div>
		</div>
		<p>{message}</p>
	</React.Fragment>
);
