import { request } from '../config/axios';
import axios from 'axios';
import { IShopStaff, IShopRecord } from '../modules/Shop';

export const getShops = (data: { page: number; limit: number }) => {
	const endpoint = `/shops?page=${data.page}&limit=${data.limit !== undefined ? data.limit : 10}`;
	return request(endpoint, 'GET', null);
};

export const getSyncData = (data: { shopId: string; date: string }) => {
	const endpoint = `/shops/${data.shopId}/sync?date=${data.date}`;
	return request(endpoint, 'GET', null);
};

export const createShop = (data: any) => {
	const endpoint = '/shops';
	return request(endpoint, 'POST', data);
};

export const updateShop = (data: { shopId: string; shopInfo: IShopRecord }) => {
	const endpoint = `/shops/${data.shopId}`;
	return request(endpoint, 'PUT', { ...data.shopInfo });
};

export const deleteShop = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}`;
	return request(endpoint, 'DELETE', null);
};

export const getCodeUrl = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/code_url`;
	return request(endpoint, 'GET', null);
};

export const addCode = (data: { shopId: string; code: string }) => {
	const endpoint = `/shops/${data.shopId}/code/${data.code}`;
	return request(endpoint, 'GET', null);
};

export const getShopRefreshTokenUrl = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/auto_refresh_token`;
	return request(endpoint, 'GET', null);
};

export const getTokenWithUrl = (data: { url: string }) => {
	axios
		.get(data.url)
		.then((res) => {
			console.log('res', res);
		})
		.catch((err) => {
			console.log('err', err);
		});
};

export const updateAccessToken = (data: {
	shopId: string;
	accessToken: string;
	expiredTime: string;
}) => {
	const endpoint = `/shops/${data.shopId}/access_token`;
	return request(endpoint, 'PUT', {
		accessToken: data.accessToken,
		expiredTime: data.expiredTime,
	});
};

export const getShopDetail = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}`;
	return request(endpoint, 'GET', null);
};

export const getShopStaff = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/staffs`;
	return request(endpoint, 'GET', null);
};

export const addShopStaff = (data: { shopId: string; staff: IShopStaff }) => {
	const endpoint = `/shops/${data.shopId}/staffs`;
	return request(endpoint, 'POST', { ...data.staff });
};

export const deleteShopStaff = (data: { shopId: string; staffId: string }) => {
	const endpoint = `/shops/${data.shopId}/staffs/${data.staffId}`;
	return request(endpoint, 'DELETE', null);
};

export const getDeliveryCountries = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/delivery_countries`;
	return request(endpoint, 'GET', null);
};

export const getShippingCarrierByLocale = (data: { shopId: string; locale: string }) => {
	const endpoint = `/shops/${data.shopId}/shipping_carriers?locale=${data.locale}`;
	return request(endpoint, 'GET', null);
};

export const getOrders = (data: {
	shopId: string;
	order: string;
	start: number;
	limit: number;
}) => {
	const endpoint = `/shops/${data.shopId}/orders?start=${data.start}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const getActionRequiredOrder = (data: {
	shopId: string;
	order: string;
	start: number;
	limit: number;
}) => {
	const endpoint = `/shops/${data.shopId}/fullfill_orders?start=${data.start}&limit=${data.limit}`;
	return request(endpoint, 'GET', null);
};

export const createFullFillOrder = (data: {
	shopId: string;
	orderId: string;
	countryCode: string;
	shippingCarrier: string;
}) => {
	const endpoint = `/shops/${data.shopId}/fullfill_orders`;
	return request(endpoint, 'POST', data);
};
