import { request } from '../config/axios';

export const getUsers = (data: { page: number; limit: number }) => {
	const endpoint = `/users/admin/users?page=${data.page}&limit=${
		data.limit !== undefined ? data.limit : 10
	}`;
	return request(endpoint, 'GET', null);
};

export const deleteUser = (data: { _id: string }) => {
	const endpoint = `/users/admin/users/${data._id}`;
	return request(endpoint, 'DELETE', null);
};

export const changeUserPassword = (data: { email: string; password: string }) => {
	const endpoint = '/users/admin/changePassword';
	return request(endpoint, 'PATCH', data);
};
