import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export const handleLogout = (): IActions.IHandleLogout => {
	return {
		type: Keys.HANDLE_LOGOUT,
	};
};

export const toggleModal = (data: { type: MAIN_LAYOUT_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type: data.type,
		},
	};
};
