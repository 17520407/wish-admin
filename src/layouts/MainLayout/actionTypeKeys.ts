/**
 * @file  Key of action will be listed here
 */

enum ActionTypeKeys {
	TOGGLE_MODAL = 'MAIN_LAYOUT/TOGGLE_MODAL',
	HANDLE_LOGOUT = 'MAIN_LAYOUT/HANDLE_LOGOUT',
}
export default ActionTypeKeys;
