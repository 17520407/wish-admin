import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as UserApi from '../../../api/user';
import { message } from 'antd';

// Handle Get Users
function* handleGetUsers(action: any) {
	try {
		const res = yield call(UserApi.getUsers, action.payload);
		if (res.status === 200) {
			yield put(actions.getUsersSuccess(res.data));
		} else {
			message.error(res.data.error.message, 3);
			throw new Error(res.data.error.message);
		}
	} catch (error) {
		yield put(actions.getUsersFail(error));
	}
}

// Handle Delete Users
function* handleDeleteUser(action: any) {
	try {
		const res = yield call(UserApi.deleteUser, action.payload);
		yield delay(200);
		if (res.status === 204) {
			message.success('Delete User Successful', 2);
			yield put(actions.deleteUserSuccess());
		} else {
			message.error(res.data.error.message, 3);
			throw new Error(res.data.error.message);
		}
	} catch (error) {
		yield put(actions.deleteUserFail(error));
	}
}

// Handle Change User Password
function* handleChangeUserPassword(action: any) {
	try {
		const res = yield call(UserApi.changeUserPassword, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Change Password Successful', 2);
			yield put(actions.changeUserPasswordSuccess());
		} else {
			message.error(res.data.error.message, 3);
			throw new Error(res.data.error.message);
		}
	} catch (error) {
		yield put(actions.changeUserPasswordFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetUsers() {
	yield takeEvery(Keys.GET_USERS, handleGetUsers);
}
function* watchDeleteUser() {
	yield takeEvery(Keys.DELETE_USER, handleDeleteUser);
}
function* watchChangeUserPassword() {
	yield takeEvery(Keys.CHANGE_USER_PASSWORD, handleChangeUserPassword);
}
/*-----------------------------------------------------------------*/
const sagas = [watchGetUsers, watchDeleteUser, watchChangeUserPassword];

export default sagas;
