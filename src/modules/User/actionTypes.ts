/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IToggleModal
	| IActions.IHandleCurrentUser
	| IActions.IGetUsers
	| IActions.IGetUsersSuccess
	| IActions.IGetUsersFail
	| IActions.IChangeUserPassword
	| IActions.IChangeUserPasswordSuccess
	| IActions.IChangeUserPasswordFail
	| IActions.IDeleteUser
	| IActions.IDeleteUserSuccess
	| IActions.IDeleteUserFail;

export default ActionTypes;
