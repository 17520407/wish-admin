import { Divider, PageHeader } from 'antd';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IUserProps } from '../model/IUserProps';
import { ModalChangeUserPassword } from './Modal';
import { UserTable } from './Table';

import './index.scss';

interface IProps extends RouteComponentProps, IUserProps {}

export const UserPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.getUsers({ page: 0, limit: 10 });
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<div className="site-layout-background">
				<div className="site-content">
					<PageHeader title="Users" className="p-0" />
					<Divider />
					<UserTable {...props} />
				</div>
			</div>
			<ModalChangeUserPassword {...props} />
		</React.Fragment>
	);
};
