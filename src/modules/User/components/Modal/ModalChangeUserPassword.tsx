import React from 'react';
import { Input, Button, Typography, Modal, Form, Divider } from 'antd';
import { UserOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { IUserProps } from '../../model/IUserProps';
import { USER_MODAL } from '../../model/IUserState';

const { Title } = Typography;

interface IProps extends IUserProps {}

export const ModalChangeUserPassword: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing, currentUserInfo, toggleModalChangeUserPassword } = props.store.UsersPage;

	React.useEffect(() => {
		if (currentUserInfo) {
			formInstance.setFieldsValue({
				email: currentUserInfo.email,
			});
		}
		// eslint-disable-next-line
	}, [currentUserInfo]);

	const onFinish = (data: { email: string; password: string }) => {
		props.actions.changeUserPassword({ email: data.email, password: data.password });
	};

	return (
		<Modal
			visible={toggleModalChangeUserPassword}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: USER_MODAL.CHANGE_PASSWORD,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Title level={4}>Change User Password</Title>
				<Form.Item
					label="Email"
					name="email"
					children={
						<Input
							placeholder="Enter email"
							prefix={<UserOutlined />}
							disabled={true}
						/>
					}
				/>
				<Form.Item
					label="Password"
					name="password"
					dependencies={['confirm']}
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
						{
							min: 6,
						},
					]}
					children={
						<Input.Password
							placeholder="Enter password"
							prefix={<LockOutlined />}
							iconRender={(visible) =>
								visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
							}
							disabled={isProcessing}
						/>
					}
				/>
				<Form.Item
					label="Confirm Password"
					name="confirm"
					dependencies={['password']}
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						({ getFieldValue }) => ({
							validator(rule, value) {
								if (!value || getFieldValue('password') === value) {
									return Promise.resolve();
								}
								return Promise.reject(
									'The two passwords that you entered do not match!'
								);
							},
						}),
					]}
				>
					<Input.Password
						prefix={<LockOutlined />}
						placeholder="Enter confirm password"
						disabled={isProcessing}
					/>
				</Form.Item>
				<Divider />

				<Button
					block={true}
					className="text-capitalize"
					type="primary"
					htmlType="submit"
					loading={isProcessing}
				>
					Update
				</Button>
			</Form>
		</Modal>
	);
};
