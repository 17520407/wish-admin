import React from 'react';
import { IColumn } from '../../../../common/interfaces';
import { Button, Table, Space, Tooltip, Popconfirm } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { USER_CLEAR, USER_MODAL } from '../../model/IUserState';
import { RouteComponentProps } from 'react-router-dom';
import { IUserProps } from '../../model/IUserProps';
import { IUserInfo } from '../../model/IUserState';

interface IProps extends IUserProps, RouteComponentProps {}

const columns = (props: IProps): IColumn[] => {
	return [
		{
			title: 'First name',
			dataIndex: 'firstName',
		},
		{
			title: 'Last name',
			dataIndex: 'lastName',
		},
		{
			title: 'Email',
			dataIndex: 'email',
		},
		{
			title: 'Action',
			dataIndex: 'operation',
			render: (text: string, record: IUserInfo) => (
				<Space align="center">
					<Tooltip placement="top" title="Change User Password">
						<Button
							type="text"
							icon={
								<EditOutlined
									className="text-primary"
									onClick={() => {
										props.actions.handleCurrentUser({
											type: 'detail',
											userInfo: record,
										});
										props.actions.toggleModal({
											type: USER_MODAL.CHANGE_PASSWORD,
										});
									}}
								/>
							}
						/>
					</Tooltip>
					<Tooltip placement="top" title="Delete">
						<Popconfirm
							title="Are you sure to delete this user?"
							cancelText="Cancel"
							okText="Yes"
							onConfirm={() => {
								props.actions.handleCurrentUser({
									userInfo: record,
									type: 'detail',
								});
								props.actions.deleteUser({
									_id: record._id,
								});
							}}
						>
							<Button type="text" icon={<DeleteOutlined className="text-danger" />} />
						</Popconfirm>
					</Tooltip>
				</Space>
			),
		},
	];
};

export const UserTable: React.FC<IProps> = (props) => {
	const {
		isLoadingUsers,
		deleteUserSuccess,
		userRecords,
		totalRecord,
		isProcessing,
	} = props.store.UsersPage;
	const [pageSize, setPageSize] = React.useState<number>(10);

	React.useEffect(() => {
		if (deleteUserSuccess) {
			props.actions.getUsers({ page: 0, limit: 10 });
			props.actions.handleClear({
				type: USER_CLEAR.DELETE_USER,
			});
		}
		// eslint-disable-next-line
	}, [deleteUserSuccess]);

	const onPageChange = (page: number, pageSize?: number) => {
		setPageSize(pageSize as number);
		props.actions.getUsers({
			page: page - 1,
			limit: pageSize,
		});
	};

	return (
		<React.Fragment>
			<Table
				columns={columns(props)}
				dataSource={userRecords}
				loading={isLoadingUsers || isProcessing}
				pagination={{
					total: totalRecord,
					pageSize,
					onChange: onPageChange,
					showSizeChanger: true,
				}}
			/>
		</React.Fragment>
	);
};
