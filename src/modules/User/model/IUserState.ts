export enum USER_MODAL {
	CHANGE_PASSWORD = 1,
}

export enum USER_CLEAR {
	DELETE_USER = 1,
}

export interface IUserInfo {
	key?: string | number;
	email: string;
	firstName: string;
	isActive: boolean;
	lastName: string;
	role: string;
	_id: string;
}
export interface IUserState {
	isProcessing: boolean;
	isLoadingUsers: boolean;
	deleteUserSuccess: boolean;
	toggleModalChangeUserPassword: boolean;
	currentUserInfo?: IUserInfo;
	userRecords: IUserInfo[];
	totalRecord: number;
}

// InitialState
export const initialState: IUserState = {
	isProcessing: false,
	isLoadingUsers: false,
	deleteUserSuccess: false,
	toggleModalChangeUserPassword: false,
	userRecords: [],
	totalRecord: 0,
};
