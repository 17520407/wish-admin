import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IUserProps {
	store: IStore;
	actions: typeof Actions;
}
