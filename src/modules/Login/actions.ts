import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IUserLoginInfo } from './model/ILoginState';

//#region User Login Actions
export const userLogin = (data: IUserLoginInfo): IActions.IUserLogin => {
	return {
		type: Keys.USER_LOGIN,
		payload: data,
	};
};

export const userLoginSuccess = (res: any): IActions.IUserLoginSuccess => {
	return {
		type: Keys.USER_LOGIN_SUCCESS,
		payload: res,
	};
};

export const userLoginFail = (res: any): IActions.IUserLoginFail => {
	return {
		type: Keys.USER_LOGIN_FAIL,
		payload: res,
	};
};
//#endregion
