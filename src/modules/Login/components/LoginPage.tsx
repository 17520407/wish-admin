import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { ILogInProps } from '../model/ILoginProps';
import { LoginForm } from './Form';
import { Row, Divider, Col } from 'antd';

import './index.scss';

interface IProps extends RouteComponentProps, ILogInProps {}

export const LoginPage: React.FC<IProps> = (props) => {
	return (
		<Row align="middle" className="auth_container">
			<Col span={6} offset={9} className="auth__content">
				<div className="text-center mb-1">
					{/* <span className="brand">Logo</span> */}
					<h5 className="mb-1 mt-3">Log In</h5>
					<p className="text-muted">Started with Wish</p>
				</div>
				<div className="auth-form">
					<LoginForm {...props} />
					<Divider />
					<p className="text-muted text-center">
						Don't have an account?{' '}
						<Link to="/register" className="text-primary">
							Register now
						</Link>
					</p>
				</div>
			</Col>
		</Row>
	);
};
