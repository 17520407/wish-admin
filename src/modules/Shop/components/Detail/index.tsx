import * as React from 'react';
import { RouteComponentProps, useParams } from 'react-router-dom';
import { PageHeader, Button, Descriptions, Typography, Divider, Tag, Spin } from 'antd';
import { PlusOutlined, RetweetOutlined } from '@ant-design/icons';
import { IShopProps } from '../../model/IShopProps';
import { IShopInfo, IShopRecord, SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';
import { StaffTable } from '../Table';
import { StaffForm, ShopForm } from '../Form';
import moment from 'moment';

const { Paragraph } = Typography;

interface IProps extends IShopProps, RouteComponentProps {}

export const ShopDetail: React.FC<IProps> = (props) => {
	const { currentShop, isLoadingShopDetail, isCreateStaffSuccess } = props.store.ShopPage;
	const { shopId } = useParams<{ shopId: string }>();
	React.useEffect(() => {
		if (!currentShop) {
			props.actions.getShopDetail({
				shopId,
			});
		}
		if (isCreateStaffSuccess) {
			props.actions.handleClear({ type: SHOP_CLEAR.CREATE_STAFF });
			props.actions.getStaff({
				shopId: currentShop?.shop._id as string,
			});
		}
	}, [currentShop?.shop._id, props.actions, shopId, currentShop, isCreateStaffSuccess]);

	const renderTokenStatus = (shop: IShopInfo) => {
		if (shop.expiredTime !== undefined) {
			const momentExpriedTime = moment(shop.expiredTime);
			return momentExpriedTime.isSame(moment(), 'day') ? (
				<Tag color="#dc3545">Expired</Tag>
			) : (
				<>
					{momentExpriedTime.isBefore(moment(), 'day') && (
						<span>1 day left token will expired</span>
					)}
					<Tag color="#2dbe60">Active</Tag>
				</>
			);
		}
		return (
			<>
				<Tag color="#6c757d">None</Tag>
			</>
		);
	};

	return (
		<React.Fragment>
			<Spin spinning={!currentShop || isLoadingShopDetail}>
				<PageHeader
					className="pb-0"
					ghost={false}
					onBack={() => {
						props.history.push('/shop');
						props.actions.handleClear({ type: SHOP_CLEAR.VIEW_DETAIL });
					}}
					title={`Detail - ${currentShop?.shop.name}`}
					extra={[
						<Button
							key="3"
							icon={<PlusOutlined />}
							onClick={() =>
								props.actions.toggleModal({ type: SHOP_MODAL.CREATE_EDIT_STAFF })
							}
							type="primary"
						>
							Add Staff
						</Button>,
						<Button
							key="2"
							icon={<RetweetOutlined />}
							onClick={() => {
								props.actions.toggleModal({
									type: SHOP_MODAL.CREATE_EDIT_SHOP,
								});
								props.actions.handleCurrentShop({
									type: 'update',
									shopInfo: currentShop as IShopRecord,
								});
							}}
							className="btn-success"
						>
							Update Shop
						</Button>,
					]}
				>
					<Descriptions size="small" column={3}>
						<Descriptions.Item label="Name">{currentShop?.shop.name}</Descriptions.Item>
						<Descriptions.Item label="Token">
							{currentShop && renderTokenStatus(currentShop?.shop as IShopInfo)}
						</Descriptions.Item>
						<Descriptions.Item label="Expired Time">
							{currentShop?.shop.expiredTime !== undefined
								? moment(currentShop?.shop.expiredTime).format(
										'DD/MM/YYYY HH:mm:ss'
										// tslint:disable-next-line: indent
								  )
								: 'None'}
						</Descriptions.Item>
						<Descriptions.Item label="Client Id">
							<Paragraph copyable={true}>{currentShop?.shop.clientId}</Paragraph>
						</Descriptions.Item>
						<Descriptions.Item label="Client Secret">
							<Paragraph copyable={true}>{currentShop?.shop.clientSecret}</Paragraph>
						</Descriptions.Item>
					</Descriptions>
				</PageHeader>
				<div className="site-content pt-0">
					<Divider>
						<span className="text-bold">Staff Table</span>
					</Divider>
					<StaffTable {...props} />
				</div>
				<StaffForm {...props} />
			</Spin>
			<ShopForm {...props} />
		</React.Fragment>
	);
};
