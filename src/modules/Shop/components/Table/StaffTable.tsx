import React from 'react';
import { IShopProps } from '../../model/IShopProps';
import { IColumn } from '../../../../common/interfaces';
import { Table, Space, Tooltip, Popconfirm } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { IShopStaff } from '../../model/IShopState';

interface IProps extends IShopProps {}

const columnsStaff = (props: IShopProps): IColumn[] => {
	return [
		{
			title: 'First name',
			dataIndex: 'key',
			render: (text: string, record: IShopStaff) => <span>{record.user?.firstName}</span>,
		},
		{
			title: 'Last name',
			dataIndex: 'key',
			render: (text: string, record: IShopStaff) => <span>{record.user?.lastName}</span>,
		},
		{
			title: 'Email',
			dataIndex: 'key',
			render: (text: string, record: IShopStaff) => <span>{record.user?.email}</span>,
		},
		{
			title: 'Role',
			dataIndex: 'role',
			render: (text: string, record: IShopStaff) => <span>{record.role}</span>,
		},
		{
			title: 'Action',
			dataIndex: 'operation',
			render: (text: any, record: IShopStaff) => {
				const { currentShop } = props.store.ShopPage;
				return (
					<Space align="center">
						<Tooltip placement="top" title="Delete">
							<Popconfirm
								title="Are you sure to delete this staff?"
								cancelText="Cancel"
								okText="Yes"
								onConfirm={() => {
									props.actions.handleCurrentStaff({
										staff: record,
										type: 'delete',
									});
									props.actions.deleteStaff({
										shopId: currentShop?.shop._id as string,
										staffId: record._id as string,
									});
								}}
							>
								<DeleteOutlined key="ellipsis" className="text-danger" />
							</Popconfirm>
						</Tooltip>
					</Space>
				);
			},
		},
	];
};

export const StaffTable: React.FC<IProps> = (props) => {
	const { staffRecords, isProcessing, isLoadingStaff } = props.store.ShopPage;

	return (
		<Table
			columns={columnsStaff(props)}
			dataSource={staffRecords}
			loading={isLoadingStaff || isProcessing}
			pagination={false}
		/>
	);
};
