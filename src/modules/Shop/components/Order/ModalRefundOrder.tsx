import React from 'react';
import { Modal, Select, Typography, Button, Form, Divider } from 'antd';
import { IShopProps } from '../../model/IShopProps';
import { ORDER_TAB, SHOP_MODAL } from '../../model/IShopState';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';

const { Title } = Typography;
const { Option } = Select;

export const ModalRefundOrder: React.FC<IShopProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { toggleModalRefundOrder, isProcessing, currentOrder } = props.store.ShopPage;
	const { search } = useLocation();
	const { type } = queryString.parse(search);

	const onFinish = (data: { code: number }) => {
		props.actions.postRefundOrder({
			shopId: currentOrder?.shopId._id as string,
			orderId: currentOrder?.order_id as string,
			reasonCode: data.code,
		});
	};

	return (
		<Modal
			visible={toggleModalRefundOrder}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: SHOP_MODAL.REFUND_ORDER,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form form={formInstance} layout="vertical" onFinish={onFinish}>
				<Title level={4}>Refund Order</Title>
				<Divider className="mt-3" />
				<Form.Item
					label="Provide a reason for the refund"
					name="code"
					rules={[{ required: true, message: 'You must provide a refund category' }]}
				>
					<Select placeholder="Please Select" disabled={isProcessing}>
						{type !== ORDER_TAB.ACTION_REQUIRED ? (
							<>
								<Option value={25}>Item is damaged</Option>
								<Option value={32}>Item was returned to sender</Option>
							</>
						) : (
							<Option value={1}>
								Store is unable to fulfill order, unable to ship
							</Option>
						)}
					</Select>
				</Form.Item>
				<div className="d-flex w-100 justify-content-end gap-2">
					<Button
						disabled={isProcessing}
						onClick={() => props.actions.toggleModal({ type: SHOP_MODAL.REFUND_ORDER })}
					>
						Cancel
					</Button>
					<Button type="primary" htmlType="submit" loading={isProcessing}>
						Refund
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
