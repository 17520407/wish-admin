import React from 'react';
import { Modal, Descriptions, Divider, Button } from 'antd';
import { IShopProps } from '../../model/IShopProps';
import { SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';

export const ModalShipAddress: React.FC<IShopProps> = (props) => {
	const { toggleModalShipAddress, currentOrder } = props.store.ShopPage;

	return (
		<Modal
			width={768}
			title="Shipping  Address"
			visible={toggleModalShipAddress}
			onCancel={() => {
				props.actions.toggleModal({
					type: SHOP_MODAL.SHIP_ADDRESS,
				});
				props.actions.handleClear({
					type: SHOP_CLEAR.SHIP_ADDRESS,
				});
			}}
			footer={[
				<Button
					key="close"
					type="primary"
					onClick={() => props.actions.toggleModal({ type: SHOP_MODAL.SHIP_ADDRESS })}
				>
					Close
				</Button>,
			]}
		>
			<Descriptions bordered={true} layout="horizontal" column={2}>
				<Descriptions.Item label="Name">
					{currentOrder?.ShippingDetail.name ? currentOrder?.ShippingDetail.name : 'None'}
				</Descriptions.Item>
				<Descriptions.Item label="State">
					{currentOrder?.ShippingDetail.state}
				</Descriptions.Item>
				<Descriptions.Item label="Street Address 1">
					{currentOrder?.ShippingDetail.street_address1}
				</Descriptions.Item>
				<Descriptions.Item label="City">
					{currentOrder?.ShippingDetail.city}
				</Descriptions.Item>
				<Descriptions.Item label="Street Address 2">
					{currentOrder?.ShippingDetail.street_address2
						? currentOrder?.ShippingDetail.street_address2
						: 'None'}
				</Descriptions.Item>
				<Descriptions.Item label="ZIP Code">
					{currentOrder?.ShippingDetail.zipcode}
				</Descriptions.Item>
				<Descriptions.Item label="Country/Region">
					{currentOrder?.ShippingDetail.country}
				</Descriptions.Item>
				<Descriptions.Item label="Phone number">
					{currentOrder?.ShippingDetail.phone_number}
				</Descriptions.Item>
			</Descriptions>
			<Divider />
			<p className="text-muted" />
			<p className="text-muted" />
			<p className="text-muted" />
			<p className="text-muted" />
		</Modal>
	);
};
