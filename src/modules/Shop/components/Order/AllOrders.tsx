import * as React from 'react';
import { RouteComponentProps, useLocation } from 'react-router-dom';
import { PageHeader, Tabs, Input, Row, Col, Pagination } from 'antd';
import queryString from 'query-string';
import { IShopProps } from '../../model/IShopProps';
import { ORDER_TAB, SHOP_CLEAR } from '../../model/IShopState';
import { OrderTable } from '../Table';
import { OrderForm } from '../Form';
import { ModalShipAddress } from './ModalShipAddress';
import { ModalProductDetail } from './ModalProductDetail';
import { ModalRefundOrder } from './ModalRefundOrder';
import { DEFAULT_LIMIT } from '../../../../common/constants';

interface IProps extends IShopProps, RouteComponentProps {}

const { TabPane } = Tabs;

export const AllOrders: React.FC<IProps> = (props) => {
	const { totalOrderRecord } = props.store.ShopPage;
	const { search } = useLocation();
	const { type, sort, sortCol, start } = queryString.parse(search);
	const [recordStart, setStart] = React.useState(0);
	const [curTab, setCurTab] = React.useState<ORDER_TAB | string>(type as string);
	const [sortType] = React.useState<number>(parseInt(sort as string, 10));

	React.useEffect(() => {
		if (type !== '' && sort !== '' && sortCol !== '' && start !== '') {
			if (Object.values(ORDER_TAB).includes(type as any)) {
				setCurTab(type as string);
				props.actions.getOrdersMultipleShop({
					start: parseInt(start as string, 10),
					type: type as ORDER_TAB,
					sort: -1,
					limit: DEFAULT_LIMIT,
					order: undefined,
					sortCol: 'last_updated',
				});
			} else {
				setCurTab(ORDER_TAB.ACTION_REQUIRED);
				setStart(0);
				props.history.push(
					`/shop/order/all?type=${ORDER_TAB.ACTION_REQUIRED}&start=0&limit=${DEFAULT_LIMIT}&sort=${sortType}&sortCol=last_updated`
				);
				props.actions.getOrdersMultipleShop({
					start: parseInt(start as string, 10),
					type: ORDER_TAB.ACTION_REQUIRED,
					sort: sortType,
					limit: DEFAULT_LIMIT,
					order: undefined,
					sortCol: 'last_updated',
				});
			}
		} else {
			props.history.push(`/shop/list`);
		}
		// eslint-disable-next-line
	}, []);

	const onTabChange = (activeKey: ORDER_TAB | string) => {
		setCurTab(activeKey);
		setStart(0);
		props.history.push(
			`/shop/orders/all?type=${activeKey}&start=0&limit=${DEFAULT_LIMIT}&sort=-1&sortCol=last_updated`
		);
		props.actions.getOrdersMultipleShop({
			start: 0,
			type: activeKey as ORDER_TAB,
			sort: -1,
			limit: DEFAULT_LIMIT,
			order: undefined,
			sortCol: 'last_updated',
		});
	};

	const onPageChange = (page: number) => {
		setStart(page - 1);
		let queryPage = page;
		queryPage = (page - 1) * DEFAULT_LIMIT;
		props.history.push(
			`/shop/orders/all?type=${curTab}&start=${queryPage}&limit=${DEFAULT_LIMIT}&sort=${sortType}&sortCol=last_updated`
		);
		props.actions.getOrdersMultipleShop({
			type: curTab as ORDER_TAB,
			sort: sortType,
			start: queryPage,
			limit: DEFAULT_LIMIT,
			order: undefined,
			sortCol: 'last_updated',
		});
	};

	return (
		<React.Fragment>
			<PageHeader
				className="pb-0"
				ghost={false}
				title="All Orders"
				onBack={() => {
					props.history.push('/shop');
					props.actions.handleClear({ type: SHOP_CLEAR.VIEW_ORDER });
				}}
			/>
			<div className="site-content pt-0">
				<Tabs defaultActiveKey={curTab} onChange={onTabChange}>
					<TabPane tab="Action Required" key={ORDER_TAB.ACTION_REQUIRED} />
					<TabPane tab="History" key={ORDER_TAB.HISTORY} />
					<TabPane tab="Note Order" key={ORDER_TAB.NOTE_ORDER} />
				</Tabs>
				<Row gutter={[8, 16]}>
					<Col span={12}>
						<div className="d-inline-flex w-100">
							<Input.Search
								allowClear={true}
								placeholder="Enter OrderID"
								onSearch={(value) => {
									props.actions.getOrdersMultipleShop({
										type: curTab as ORDER_TAB,
										sort: -1,
										start: parseInt(start as string, 10),
										limit: DEFAULT_LIMIT,
										order: value !== '' ? value : undefined,
										sortCol: 'last_updated',
									});
								}}
							/>
						</div>
					</Col>
				</Row>
				<div>
					<OrderTable
						{...props}
						onPageChange={onPageChange}
						curTab={curTab}
						start={parseInt(start as string, 10)}
					/>
				</div>
				<div className="d-flex justify-content-end mt-4">
					<span className="d-flex align-items-center px-3">
						{(recordStart + 1) * DEFAULT_LIMIT < totalOrderRecord
							? (recordStart + 1) * DEFAULT_LIMIT
							: totalOrderRecord}
						/{totalOrderRecord}
					</span>
					<Pagination
						current={recordStart + 1}
						defaultCurrent={1}
						total={totalOrderRecord}
						pageSize={50}
						onChange={onPageChange}
						showSizeChanger={false}
					/>
				</div>
			</div>
			<OrderForm {...props} />
			<ModalShipAddress {...props} />
			<ModalProductDetail {...props} />
			<ModalRefundOrder {...props} />
		</React.Fragment>
	);
};
