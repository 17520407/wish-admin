import React from 'react';
import { Modal, Col, Row, Typography, Button } from 'antd';
import { IShopProps } from '../../model/IShopProps';
import { SHOP_MODAL } from '../../model/IShopState';
const { Title } = Typography;

export const ModalProductDetail: React.FC<IShopProps> = (props) => {
	const { toggleModalProductDetail, currentOrder } = props.store.ShopPage;

	return (
		<Modal
			visible={toggleModalProductDetail}
			onCancel={() => {
				props.actions.toggleModal({
					type: SHOP_MODAL.PRODUCT_DETAIL,
				});
			}}
			footer={[
				<Button
					key="close"
					type="primary"
					onClick={() => props.actions.toggleModal({ type: SHOP_MODAL.PRODUCT_DETAIL })}
				>
					Close
				</Button>,
			]}
		>
			<Title level={4}>Product Detail</Title>
			<Row gutter={[16, 16]}>
				<Col lg={12}>
					<img
						src={currentOrder?.product_image_url}
						alt={`product_detail__${currentOrder?.product_id}`}
					/>
				</Col>
				<Col lg={12}>
					<div className="mb-2">
						<Title level={5} className="mb-0" style={{ display: 'inline-block' }}>
							Name:
						</Title>
						&nbsp;
						<span>{currentOrder?.product_name}</span>
					</div>
					<div>
						<Title level={5} className="mb-0" style={{ display: 'inline-block' }}>
							ID:
						</Title>
						&nbsp;
						<span>{currentOrder?.product_id}</span>
					</div>
				</Col>
			</Row>
			<div>
				<Title level={5}>Description</Title>
			</div>
		</Modal>
	);
};
