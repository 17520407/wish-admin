import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IShopProps } from '../model/IShopProps';
import { ShopSubRoutes } from './ShopSubRoutes';

import './index.scss';

interface IProps extends RouteComponentProps, IShopProps {}

export const ShopPage: React.FC<IProps> = (props) => {
	return (
		<div className="site-layout-background">
			<ShopSubRoutes {...props} />
		</div>
	);
};
