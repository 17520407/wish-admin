export enum SHOP_MODAL {
	CREATE_EDIT_SHOP = 1,
	CREATE_EDIT_STAFF = 2,
	ADD_CODE = 3,
	SHIP_ORDER = 4,
	SHIP_ADDRESS = 5,
	PRODUCT_DETAIL = 6,
	REFUND_ORDER = 7,
}

export enum SHOP_VIEW {
	VIEW_LIST = 0,
	VIEW_DETAIL = 1,
	VIEW_ORDER = 2,
}

export enum SHOP_CLEAR {
	UPDATE_SHOP = 1,
	CREATE_STAFF = 2,
	VIEW_DETAIL = 3,
	VIEW_ORDER = 4,
	SHIP_ADDRESS = 5,
	ORDER = 6,
	SYNC_DATA = 7,
	GET_SHOP = 8,
	SHIP_REFUND_ORDER = 9,
}

export enum ORDER_TAB {
	HISTORY = 'history',
	ACTION_REQUIRED = 'action_required',
	NOTE_ORDER = 'noted',
}

export interface IOrderRecord {
	key?: string | number;
	isNoted: boolean;
	ShippingDetail: {
		city: string;
		country: string;
		name: string;
		phone_number: string;
		state: string;
		street_address1: string;
		street_address2: string;
		zipcode: string;
	};
	advanced_logistics: string;
	china_post_subsidy_amount: string;
	confirmed_delivery: string;
	cost: string;
	currency_code: string;
	days_to_fulfill: string;
	expected_ship_date: string;
	fine_ids: any[];
	hours_to_fulfill: string;
	is_combined_order: string;
	is_fbs_order: string;
	is_fbw: string;
	is_wish_express: string;
	last_updated: string;
	optional_advanced_logistics: string;
	order_id: string;
	order_time: string;
	order_total: string;
	pay_customer_vat_required: string;
	premiumcarrier_upgraded: string;
	price: string;
	product_id: string;
	shopId: {
		_id: string;
		name: string;
	};
	product_image_url: string;
	product_name: string;
	quantity: string;
	released_to_merchant_time: string;
	requires_delivered_duty_paid: string;
	requires_delivery_confirmation: string;
	shipping: string;
	shipping_cost: string;
	sku: string;
	state: 'APPROVED' | 'SHIPPED' | 'REFUNDED';
	tracking_confirmed: string;
	transaction_id: string;
	tracking_number: string;
	variant_id: string;
	wish_express_tier: '';
}

export interface IOrder {
	_id?: string;
	orderId: string;
	countryCode: string;
	shippingCarrier: string;
	trackingNumber: string;
}

export interface IShopInfo {
	_id?: string;
	name: string;
	clientId: string;
	clientSecret: string;
	expiredTime?: string | undefined;
	lastSync?: string;
	syncStatus: 'SCHEDULE' | 'UN_SYNC';
}

export interface IShopStaff {
	key?: string | number;
	email: string;
	role: string;
	_id?: string;
	user?: {
		email: string;
		firstName: string;
		lastName: string;
		isActive: boolean;
		_id: string;
	};
}

export interface IShopRecord {
	key?: string | number;
	shop: IShopInfo;
	role: string;
	user: string;
	_id: string;
}

export interface IShopState {
	toggleModalCreateEditShop: boolean;
	toggleModalCreateEditStaff: boolean;
	toggleModalShipOrder: boolean;
	toggleModalAddCode: boolean;
	toggleModalShipAddress: boolean;
	toggleModalProductDetail: boolean;
	toggleModalRefundOrder: boolean;
	isLoadingShop: boolean;
	isLoadingStaff: boolean;
	isLoadingOrder: boolean;
	isLoadingDeliveryCountries: boolean;
	isLoadingShippingCarrier: boolean;
	isLoadingShopDetail: boolean;
	isGettingProductImageUrl: boolean;
	isShipOrderSuccess: boolean;
	isRefundOrderSuccess: boolean;
	isSyncingData: boolean;
	isSyncDataSuccess: boolean;
	isUpdatingShop: boolean;
	isUpdateShopSuccess: boolean;
	isRefreshingToken: boolean;
	isCreateStaffSuccess: boolean;
	isGettingShopSuccess: boolean;
	isProcessing: boolean;
	shopRecords: IShopRecord[];
	staffRecords: IShopStaff[];
	deliveryCountries: string[];
	shippingCarriers: string[];
	orderRecords: IOrderRecord[];
	currentShop: undefined | IShopRecord;
	currentStaff: undefined | IShopStaff;
	currentOrder: undefined | IOrderRecord;
	currentView: SHOP_VIEW;
	currentPage: number;
	totalOrderRecord: number;
	shippingCarriesLength: number;
	totalRecord: number;
}

// InitialState
export const initialState: IShopState = {
	toggleModalCreateEditShop: false,
	toggleModalCreateEditStaff: false,
	toggleModalShipOrder: false,
	toggleModalAddCode: false,
	toggleModalShipAddress: false,
	toggleModalProductDetail: false,
	toggleModalRefundOrder: false,
	isLoadingShop: false,
	isLoadingStaff: false,
	isLoadingOrder: false,
	isLoadingDeliveryCountries: false,
	isLoadingShopDetail: false,
	isLoadingShippingCarrier: false,
	isGettingProductImageUrl: false,
	isSyncingData: false,
	isSyncDataSuccess: false,
	isUpdatingShop: false,
	isUpdateShopSuccess: false,
	isCreateStaffSuccess: false,
	isGettingShopSuccess: false,
	isShipOrderSuccess: false,
	isRefundOrderSuccess: false,
	isRefreshingToken: false,
	isProcessing: false,
	currentView: SHOP_VIEW.VIEW_LIST,
	currentShop: undefined,
	currentStaff: undefined,
	currentOrder: undefined,
	shopRecords: [],
	staffRecords: [],
	orderRecords: [],
	deliveryCountries: [],
	shippingCarriers: [],
	currentPage: 0,
	totalOrderRecord: 0,
	totalRecord: 0,
	shippingCarriesLength: 0,
};
