/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IHandleCurrentShop
	| IActions.IHandleCurrentStaff
	| IActions.IHandleCurrentOrder
	| IActions.IToggleModal
	| IActions.IToggleView
	| IActions.IGetShops
	| IActions.IGetShopsSuccess
	| IActions.IGetShopsFail
	| IActions.IGetSyncData
	| IActions.IGetSyncDataSuccess
	| IActions.IGetSyncDataFail
	| IActions.IGetShopDetail
	| IActions.IGetShopDetailSuccess
	| IActions.IGetShopDetailFail
	| IActions.ICreateShop
	| IActions.ICreateShopSuccess
	| IActions.ICreateShopFail
	| IActions.IUpdateShop
	| IActions.IUpdateShopSuccess
	| IActions.IUpdateShopFail
	| IActions.IDeleteShop
	| IActions.IDeleteShopSuccess
	| IActions.IDeleteShopFail
	| IActions.IGetCodeUrl
	| IActions.IGetCodeUrlSuccess
	| IActions.IGetCodeUrlFail
	| IActions.IAddCode
	| IActions.IAddCodeSuccess
	| IActions.IAddCodeFail
	| IActions.IGetStaff
	| IActions.IGetStaffSuccess
	| IActions.IGetStaffFail
	| IActions.ICreateStaff
	| IActions.ICreateStaffSuccess
	| IActions.ICreateStaffFail
	| IActions.IDeleteStaff
	| IActions.IDeleteStaffSuccess
	| IActions.IDeleteStaffFail
	| IActions.IGetRefreshTokenUrl
	| IActions.IGetRefreshTokenUrlSuccess
	| IActions.IGetRefreshTokenUrlFail
	| IActions.IGetOrders
	| IActions.IGetOrdersSuccess
	| IActions.IGetOrdersFail
	| IActions.IGetOrdersMultipleShop
	| IActions.IGetOrdersMultipleShopSuccess
	| IActions.IGetOrdersMultipleShopFail
	| IActions.IGetActionRequiredOrders
	| IActions.IGetActionRequiredOrdersSuccess
	| IActions.IGetActionRequiredOrdersFail
	| IActions.IGetNoteOrders
	| IActions.IGetNoteOrdersSuccess
	| IActions.IGetNoteOrdersFail
	| IActions.IPutNoteOrder
	| IActions.IPutNoteOrderSuccess
	| IActions.IPutNoteOrderFail
	| IActions.IPostFullFillOrder
	| IActions.IPostFullFillOrderSuccess
	| IActions.IPostFullFillOrderFail
	| IActions.IPostRefundOrder
	| IActions.IPostRefundOrderSuccess
	| IActions.IPostRefundOrderFail
	| IActions.IPostModifyShippedOrder
	| IActions.IPostModifyShippedOrderSuccess
	| IActions.IPostModifyShippedOrderFail
	| IActions.IGetDeliveryCountries
	| IActions.IGetDeliveryCountriesSuccess
	| IActions.IGetDeliveryCountriesFail
	| IActions.IGetShippingCarrierByLocale
	| IActions.IGetShippingCarrierByLocaleSuccess
	| IActions.IGetShippingCarrierByLocaleFail
	| IActions.IGetOriginalProductImage
	| IActions.IGetOriginalProductImageSuccess
	| IActions.IGetOriginalProductImageFail;

export default ActionTypes;
