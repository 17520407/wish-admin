import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IShopState, initialState, SHOP_MODAL, SHOP_CLEAR } from './model/IShopState';
import { downloadImage } from '../../services/downloadImage';

export const name = 'ShopPage';

export const reducer: Reducer<IShopState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);
		case Keys.TOGGLE_VIEW:
			return onToggleView(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_SHOP:
			return onHandleCurrentShop(state, action);

		case Keys.HANDLE_CURRENT_ORDER:
			return onHandleCurrentOrder(state, action);

		case Keys.HANDLE_CURRENT_STAFF:
			return onHandleCurrentStaff(state, action);

		case Keys.GET_SHOPS:
			return onGetShops(state, action);
		case Keys.GET_SHOPS_SUCCESS:
			return onGetShopsSuccess(state, action);
		case Keys.GET_SHOPS_FAIL:
			return onGetShopsFail(state, action);

		case Keys.GET_SYNC_DATA:
			return onGetSyncData(state, action);
		case Keys.GET_SYNC_DATA_SUCCESS:
			return onGetSyncDataSuccess(state, action);
		case Keys.GET_SYNC_DATA_FAIL:
			return onGetSyncDataFail(state, action);

		case Keys.GET_SHOP_DETAIL:
			return onGetShopDetail(state, action);
		case Keys.GET_SHOP_DETAIL_SUCCESS:
			return onGetShopDetailSuccess(state, action);
		case Keys.GET_SHOP_DETAIL_FAIL:
			return onGetShopDetailFail(state, action);

		case Keys.CREATE_SHOP:
			return onCreateShop(state, action);
		case Keys.CREATE_SHOP_SUCCESS:
			return onCreateShopSuccess(state, action);
		case Keys.CREATE_SHOP_FAIL:
			return onCreateShopFail(state, action);

		case Keys.UPDATE_SHOP:
			return onUpdateShop(state, action);
		case Keys.UPDATE_SHOP_SUCCESS:
			return onUpdateShopSuccess(state, action);
		case Keys.UPDATE_SHOP_FAIL:
			return onUpdateShopFail(state, action);

		case Keys.DELETE_SHOP:
			return onDeleteShop(state, action);
		case Keys.DELETE_SHOP_SUCCESS:
			return onDeleteShopSuccess(state, action);
		case Keys.DELETE_SHOP_FAIL:
			return onDeleteShopFail(state, action);

		case Keys.GET_CODE_URL:
			return onGetCodeUrl(state, action);
		case Keys.GET_CODE_URL_SUCCESS:
			return onGetCodeUrlSuccess(state, action);
		case Keys.GET_CODE_URL_FAIL:
			return onGetCodeUrlFail(state, action);

		case Keys.GET_REFRESH_TOKEN_URL:
			return onGetRefreshTokenUrl(state, action);
		case Keys.GET_REFRESH_TOKEN_URL_SUCCESS:
			return onGetRefreshTokenUrlSuccess(state, action);
		case Keys.GET_REFRESH_TOKEN_URL_FAIL:
			return onGetRefreshTokenUrlFail(state, action);

		case Keys.ADD_CODE:
			return onAddCode(state, action);
		case Keys.ADD_CODE_SUCCESS:
			return onAddCodeSuccess(state, action);
		case Keys.ADD_CODE_FAIL:
			return onAddCodeFail(state, action);

		case Keys.GET_STAFF:
			return onGetStaff(state, action);
		case Keys.GET_STAFF_SUCCESS:
			return onGetStaffSuccess(state, action);
		case Keys.GET_STAFF_FAIL:
			return onGetStaffFail(state, action);

		case Keys.CREATE_STAFF:
			return onCreateStaff(state, action);
		case Keys.CREATE_STAFF_SUCCESS:
			return onCreateStaffSuccess(state, action);
		case Keys.CREATE_STAFF_FAIL:
			return onCreateStaffFail(state, action);

		case Keys.DELETE_STAFF:
			return onDeleteStaff(state, action);
		case Keys.DELETE_STAFF_SUCCESS:
			return onDeleteStaffSuccess(state, action);
		case Keys.DELETE_STAFF_FAIL:
			return onDeleteStaffFail(state, action);

		case Keys.GET_ORDERS:
			return onGetOrders(state, action);
		case Keys.GET_ORDERS_SUCCESS:
			return onGetOrdersSuccess(state, action);
		case Keys.GET_ORDERS_FAIL:
			return onGetOrdersFail(state, action);

		case Keys.GET_ORDERS_MULTIPLE_SHOP:
			return onGetOrdersMultipleShop(state, action);
		case Keys.GET_ORDERS_MULTIPLE_SHOP_SUCCESS:
			return onGetOrdersMultipleShopSuccess(state, action);
		case Keys.GET_ORDERS_MULTIPLE_SHOP_FAIL:
			return onGetOrdersMultipleShopFail(state, action);

		case Keys.GET_ACTION_REQUIRED_ORDERS:
			return onGetActionRequiredOrders(state, action);
		case Keys.GET_ACTION_REQUIRED_ORDERS_SUCCESS:
			return onGetActionRequiredOrdersSuccess(state, action);
		case Keys.GET_ACTION_REQUIRED_ORDERS_FAIL:
			return onGetActionRequiredOrdersFail(state, action);

		case Keys.GET_NOTE_ORDERS:
			return onGetNoteOrders(state, action);
		case Keys.GET_NOTE_ORDERS_SUCCESS:
			return onGetNoteOrdersSuccess(state, action);
		case Keys.GET_NOTE_ORDERS_FAIL:
			return onGetNoteOrdersFail(state, action);

		case Keys.POST_FULL_FILL_ORDERS:
			return onPostFullFillOrder(state, action);
		case Keys.POST_FULL_FILL_ORDERS_SUCCESS:
			return onPostFullFillOrderSuccess(state, action);
		case Keys.POST_FULL_FILL_ORDERS_FAIL:
			return onPostFullFillOrderFail(state, action);

		case Keys.POST_REFUND_ORDER:
			return onPostRefundOrder(state, action);
		case Keys.POST_REFUND_ORDER_SUCCESS:
			return onPostRefundOrderSuccess(state, action);
		case Keys.POST_REFUND_ORDER_FAIL:
			return onPostRefundOrderFail(state, action);

		case Keys.POST_MODIFY_SHIPPED_ORDER:
			return onPostModifyShippedOrder(state, action);
		case Keys.POST_MODIFY_SHIPPED_ORDER_SUCCESS:
			return onPostModifyShippedOrderSuccess(state, action);
		case Keys.POST_MODIFY_SHIPPED_ORDER_FAIL:
			return onPostModifyShippedOrderFail(state, action);

		case Keys.PUT_NOTE_ORDER:
			return onPutNoteOrder(state, action);
		case Keys.PUT_NOTE_ORDER_SUCCESS:
			return onPutNoteOrderSuccess(state, action);
		case Keys.PUT_NOTE_ORDER_FAIL:
			return onPutNoteOrderFail(state, action);

		case Keys.GET_DELIVERY_COUNTRIES:
			return onGetDeliveryCountries(state, action);
		case Keys.GET_DELIVERY_COUNTRIES_SUCCESS:
			return onGetDeliveryCountriesSuccess(state, action);
		case Keys.GET_DELIVERY_COUNTRIES_FAIL:
			return onGetDeliveryCountriesFail(state, action);

		case Keys.GET_SHIPPING_CARRIER_BY_LOCALE:
			return onGetShippingCarrierByLocale(state, action);
		case Keys.GET_SHIPPING_CARRIER_BY_LOCALE_SUCCESS:
			return onGetShippingCarrierByLocaleSuccess(state, action);
		case Keys.GET_SHIPPING_CARRIER_BY_LOCALE_FAIL:
			return onGetShippingCarrierByLocaleFail(state, action);

		case Keys.GET_ORIGINAL_PRODUCT_IMAGE:
			return onGetOriginalProductImage(state, action);
		case Keys.GET_ORIGINAL_PRODUCT_IMAGE_SUCCESS:
			return onGetOriginalProductImageSuccess(state, action);
		case Keys.GET_ORIGINAL_PRODUCT_IMAGE_FAIL:
			return onGetOriginalProductImageFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action

const onHandleClear = (state: IShopState, action: IActions.IHandleClear) => {
	const { type } = action.payload;

	switch (type) {
		case SHOP_CLEAR.GET_SHOP:
			return {
				...state,
				isGettingShopSuccess: false,
			};
		case SHOP_CLEAR.UPDATE_SHOP:
			return {
				...state,
				isUpdatingShop: false,
				isUpdateShopSuccess: false,
			};
		case SHOP_CLEAR.CREATE_STAFF:
			return {
				...state,
				isCreateStaffSuccess: false,
			};
		case SHOP_CLEAR.VIEW_DETAIL:
			return {
				...state,
				currentStaff: undefined,
				currentShop: undefined,
			};
		case SHOP_CLEAR.VIEW_ORDER:
			return {
				...state,
				currentShop: undefined,
				currentOrder: undefined,
				isProcessing: false,
			};
		case SHOP_CLEAR.ORDER:
			return {
				...state,
				currentOrder: undefined,
				isLoadingDeliveryCountries: false,
				isLoadingShippingCarrier: false,
				isProcessing: false,
			};
		case SHOP_CLEAR.SHIP_ADDRESS:
			return {
				...state,
				currentOrder: undefined,
			};
		case SHOP_CLEAR.SYNC_DATA:
			return {
				...state,
				isSyncDataSuccess: false,
				currentShop: undefined,
			};
		case SHOP_CLEAR.SHIP_REFUND_ORDER:
			return {
				...state,
				isShipOrderSuccess: false,
				isRefundOrderSuccess: false,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleCurrentShop = (state: IShopState, action: IActions.IHandleCurrentShop) => {
	const { shopInfo, type } = action.payload;
	const shopRecords = [...state.shopRecords];

	switch (type) {
		case 'update':
			return {
				...state,
				isUpdatingShop: true,
				currentShop: shopInfo,
			};
		case 'sync':
			const index = shopRecords.findIndex((item) => item._id === shopInfo._id);
			if (index > -1) {
				shopRecords[index] = shopInfo;
			}
			return {
				...state,
				shopRecords,
			};
		default:
			return {
				...state,
				currentShop: shopInfo,
			};
	}
};

const onHandleCurrentStaff = (state: IShopState, action: IActions.IHandleCurrentStaff) => {
	const { staff, type } = action.payload;

	switch (type) {
		default:
			return {
				...state,
				currentStaff: staff,
			};
	}
};

const onHandleCurrentOrder = (state: IShopState, action: IActions.IHandleCurrentOrder) => {
	const { order, type } = action.payload;

	switch (type) {
		case 'ship':
			return {
				...state,
				currentOrder: order,
				toggleModalShipOrder: true,
			};

		default:
			return {
				...state,
				currentOrder: order,
			};
	}
};

const onToggleModal = (state: IShopState, action: IActions.IToggleModal) => {
	const { type } = action.payload;

	switch (type) {
		case SHOP_MODAL.SHIP_ADDRESS:
			return {
				...state,
				toggleModalShipAddress: !state.toggleModalShipAddress,
			};
		case SHOP_MODAL.PRODUCT_DETAIL:
			return {
				...state,
				toggleModalProductDetail: !state.toggleModalProductDetail,
			};
		case SHOP_MODAL.CREATE_EDIT_SHOP:
			return {
				...state,
				toggleModalCreateEditShop: !state.toggleModalCreateEditShop,
			};
		case SHOP_MODAL.ADD_CODE:
			return {
				...state,
				toggleModalAddCode: !state.toggleModalAddCode,
			};
		case SHOP_MODAL.CREATE_EDIT_STAFF:
			return {
				...state,
				toggleModalCreateEditStaff: !state.toggleModalCreateEditStaff,
			};
		case SHOP_MODAL.SHIP_ORDER:
			return {
				...state,
				toggleModalShipOrder: !state.toggleModalShipOrder,
			};
		case SHOP_MODAL.REFUND_ORDER:
			return {
				...state,
				toggleModalRefundOrder: !state.toggleModalRefundOrder,
			};
		default:
			return {
				...state,
			};
	}
};

const onToggleView = (state: IShopState, action: IActions.IToggleView) => {
	const { type } = action.payload;
	return {
		...state,
		currentView: type,
	};
};

const onGetShops = (state: IShopState, action: IActions.IGetShops) => {
	return {
		...state,
		isLoadingShop: true,
	};
};
const onGetShopsSuccess = (state: IShopState, action: IActions.IGetShopsSuccess) => {
	const { items, current_page, total_item } = action.payload;
	items.forEach((record, index) => {
		record['key'] = index;
	});

	return {
		...state,
		isLoadingShop: false,
		shopRecords: items,
		currentPage: current_page,
		totalRecord: total_item,
		isGettingShopSuccess: true,
	};
};
const onGetShopsFail = (state: IShopState, action: IActions.IGetShopsFail) => {
	return {
		...state,
		isLoadingShop: false,
	};
};

const onGetSyncData = (state: IShopState, action: IActions.IGetSyncData) => {
	return {
		...state,
		isSyncingData: true,
	};
};
const onGetSyncDataSuccess = (state: IShopState, action: IActions.IGetSyncDataSuccess) => {
	return {
		...state,
		isSyncingData: false,
		isSyncDataSuccess: true,
	};
};
const onGetSyncDataFail = (state: IShopState, action: IActions.IGetSyncDataFail) => {
	return {
		...state,
		isSyncingData: false,
	};
};

const onGetShopDetail = (state: IShopState, action: IActions.IGetShopDetail) => {
	return {
		...state,
		isLoadingShopDetail: true,
	};
};
const onGetShopDetailSuccess = (state: IShopState, action: IActions.IGetShopDetailSuccess) => {
	return {
		...state,
		isLoadingShopDetail: false,
		currentShop: action.payload,
	};
};
const onGetShopDetailFail = (state: IShopState, action: IActions.IGetShopDetailFail) => {
	return {
		...state,
		isLoadingShopDetail: false,
	};
};

const onCreateShop = (state: IShopState, action: IActions.ICreateShop) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateShopSuccess = (state: IShopState, action: IActions.ICreateShopSuccess) => {
	return {
		...state,
		isProcessing: false,
		toggleModalCreateEditShop: false,
	};
};
const onCreateShopFail = (state: IShopState, action: IActions.ICreateShopFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onUpdateShop = (state: IShopState, action: IActions.IUpdateShop) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateShopSuccess = (state: IShopState, action: IActions.IUpdateShopSuccess) => {
	return {
		...state,
		isProcessing: false,
		isUpdateShopSuccess: true,
		toggleModalCreateEditShop: false,
	};
};
const onUpdateShopFail = (state: IShopState, action: IActions.IUpdateShopFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteShop = (state: IShopState, action: IActions.IDeleteShop) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteShopSuccess = (state: IShopState, action: IActions.IDeleteShopSuccess) => {
	const currentShop = state.currentShop;
	const shopRecords = [...state.shopRecords];
	const index = shopRecords.findIndex((record) => record._id === currentShop?._id);
	shopRecords.splice(index, 1);

	return {
		...state,
		isProcessing: false,
		shopRecords,
		currentShop: undefined,
	};
};
const onDeleteShopFail = (state: IShopState, action: IActions.IDeleteShopFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onGetCodeUrl = (state: IShopState, action: IActions.IGetCodeUrl) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onGetCodeUrlSuccess = (state: IShopState, action: IActions.IGetCodeUrlSuccess) => {
	const { url } = action.payload;
	window.open(url, '_blank');
	return {
		...state,
		isProcessing: false,
	};
};
const onGetCodeUrlFail = (state: IShopState, action: IActions.IGetCodeUrlFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onAddCode = (state: IShopState, action: IActions.IAddCode) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onAddCodeSuccess = (state: IShopState, action: IActions.IAddCodeSuccess) => {
	return {
		...state,
		isProcessing: false,
		toggleModalAddCode: false,
	};
};
const onAddCodeFail = (state: IShopState, action: IActions.IAddCodeFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onGetStaff = (state: IShopState, action: IActions.IGetStaff) => {
	return {
		...state,
		isLoadingStaff: true,
	};
};
const onGetStaffSuccess = (state: IShopState, action: IActions.IGetStaffSuccess) => {
	const staffRecords = action.payload;
	staffRecords.forEach((record, index) => {
		record['key'] = index;
	});

	return {
		...state,
		isLoadingStaff: false,
		staffRecords,
	};
};
const onGetStaffFail = (state: IShopState, action: IActions.IGetStaffFail) => {
	return {
		...state,
		isLoadingStaff: false,
	};
};

const onCreateStaff = (state: IShopState, action: IActions.ICreateStaff) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onCreateStaffSuccess = (state: IShopState, action: IActions.ICreateStaffSuccess) => {
	return {
		...state,
		isProcessing: false,
		isCreateStaffSuccess: true,
		toggleModalCreateEditStaff: false,
	};
};
const onCreateStaffFail = (state: IShopState, action: IActions.ICreateStaffFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onDeleteStaff = (state: IShopState, action: IActions.IDeleteStaff) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteStaffSuccess = (state: IShopState, action: IActions.IDeleteStaffSuccess) => {
	const staffRecords = [...state.staffRecords];
	const index = staffRecords.findIndex((record) => record._id === state.currentStaff?._id);
	if (index > -1) {
		staffRecords.splice(index, 1);
	}
	return {
		...state,
		isProcessing: false,
		staffRecords,
		currentStaff: undefined,
	};
};
const onDeleteStaffFail = (state: IShopState, action: IActions.IDeleteStaffFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onGetOrders = (state: IShopState, action: IActions.IGetOrders) => {
	return {
		...state,
		isLoadingOrder: true,
		orderRecords: [],
	};
};
const onGetOrdersSuccess = (state: IShopState, action: IActions.IGetOrdersSuccess) => {
	const { count, data } = action.payload;
	const orderRecords = data;
	orderRecords.forEach((record, index) => {
		if (record['hours_to_fulfill'] === undefined) {
			record['hours_to_fulfill'] = '0';
		}
		record['key'] = index;
	});

	return {
		...state,
		isLoadingOrder: false,
		orderRecords,
		recordPerPage: count,
	};
};
const onGetOrdersFail = (state: IShopState, action: IActions.IGetOrdersFail) => {
	return {
		...state,
		isLoadingOrder: false,
	};
};

const onGetOrdersMultipleShop = (state: IShopState, action: IActions.IGetOrdersMultipleShop) => {
	return {
		...state,
		isLoadingOrder: true,
		orderRecords: [],
	};
};
const onGetOrdersMultipleShopSuccess = (
	state: IShopState,
	action: IActions.IGetOrdersMultipleShopSuccess
) => {
	const { count, data } = action.payload;
	// const orderRecords = data.sort(
	// 	(a, b) => moment(b.order_time).unix() - moment(a.order_time).unix()
	// );
	const orderRecords = data;
	orderRecords.forEach((record, index) => {
		if (record['hours_to_fulfill'] === undefined) {
			record['hours_to_fulfill'] = '0';
		}
		record['key'] = index;
	});

	return {
		...state,
		isLoadingOrder: false,
		orderRecords,
		totalOrderRecord: count,
	};
};
const onGetOrdersMultipleShopFail = (
	state: IShopState,
	action: IActions.IGetOrdersMultipleShopFail
) => {
	return {
		...state,
		isLoadingOrder: false,
	};
};

const onGetActionRequiredOrders = (
	state: IShopState,
	action: IActions.IGetActionRequiredOrders
) => {
	return {
		...state,
		isLoadingOrder: true,
		orderRecords: [],
	};
};
const onGetActionRequiredOrdersSuccess = (
	state: IShopState,
	action: IActions.IGetActionRequiredOrdersSuccess
) => {
	const { count, data } = action.payload;
	const orderRecords = data;
	orderRecords.forEach((record, index) => {
		if (record['hours_to_fulfill'] === undefined) {
			record['hours_to_fulfill'] = '0';
		}
		record['key'] = index;
	});
	return {
		...state,
		isLoadingOrder: false,
		orderRecords,
		totalOrderRecord: count,
	};
};
const onGetActionRequiredOrdersFail = (
	state: IShopState,
	action: IActions.IGetActionRequiredOrdersFail
) => {
	return {
		...state,
		isLoadingOrder: false,
	};
};

const onPostFullFillOrder = (state: IShopState, action: IActions.IPostFullFillOrder) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPostFullFillOrderSuccess = (
	state: IShopState,
	action: IActions.IPostFullFillOrderSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalShipOrder: false,
		isShipOrderSuccess: true,
	};
};
const onPostFullFillOrderFail = (state: IShopState, action: IActions.IPostFullFillOrderFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onPostRefundOrder = (state: IShopState, action: IActions.IPostRefundOrder) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPostRefundOrderSuccess = (state: IShopState, action: IActions.IPostRefundOrderSuccess) => {
	return {
		...state,
		isProcessing: false,
		toggleModalRefundOrder: false,
		isRefundOrderSuccess: true,
	};
};
const onPostRefundOrderFail = (state: IShopState, action: IActions.IPostRefundOrderFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onPostModifyShippedOrder = (state: IShopState, action: IActions.IPostModifyShippedOrder) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPostModifyShippedOrderSuccess = (
	state: IShopState,
	action: IActions.IPostModifyShippedOrderSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalShipOrder: false,
	};
};
const onPostModifyShippedOrderFail = (
	state: IShopState,
	action: IActions.IPostModifyShippedOrderFail
) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onGetNoteOrders = (state: IShopState, action: IActions.IGetNoteOrders) => {
	return {
		...state,
		isLoadingOrder: true,
		orderRecords: [],
	};
};
const onGetNoteOrdersSuccess = (state: IShopState, action: IActions.IGetNoteOrdersSuccess) => {
	const { count, data } = action.payload;
	const orderRecords = data;
	orderRecords.forEach((record, index) => {
		if (record['hours_to_fulfill'] === undefined) {
			record['hours_to_fulfill'] = '0';
		}
		record['key'] = index;
	});
	return {
		...state,
		isLoadingOrder: false,
		orderRecords,
		recordPerPage: count,
	};
};
const onGetNoteOrdersFail = (state: IShopState, action: IActions.IGetNoteOrdersFail) => {
	return {
		...state,
		isLoadingOrder: false,
	};
};

const onPutNoteOrder = (state: IShopState, action: IActions.IPutNoteOrder) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPutNoteOrderSuccess = (state: IShopState, action: IActions.IPutNoteOrderSuccess) => {
	const currentOrder = state.currentOrder;
	const orderRecords = [...state.orderRecords];
	const index = orderRecords.findIndex((item) => item.order_id === currentOrder?.order_id);
	if (index > -1) {
		orderRecords[index].isNoted = !currentOrder?.isNoted;
	}
	return {
		...state,
		isProcessing: false,
		currentOrder: undefined,
		orderRecords,
	};
};
const onPutNoteOrderFail = (state: IShopState, action: IActions.IPutNoteOrderFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onGetDeliveryCountries = (state: IShopState, action: IActions.IGetDeliveryCountries) => {
	return {
		...state,
		isLoadingDeliveryCountries: true,
	};
};
const onGetDeliveryCountriesSuccess = (
	state: IShopState,
	action: IActions.IGetDeliveryCountriesSuccess
) => {
	return {
		...state,
		isLoadingDeliveryCountries: false,
		deliveryCountries: action.payload.countries,
	};
};
const onGetDeliveryCountriesFail = (
	state: IShopState,
	action: IActions.IGetDeliveryCountriesFail
) => {
	return {
		...state,
		isLoadingDeliveryCountries: false,
	};
};

const onGetShippingCarrierByLocale = (
	state: IShopState,
	action: IActions.IGetShippingCarrierByLocale
) => {
	return {
		...state,
		isLoadingShippingCarrier: true,
	};
};
const onGetShippingCarrierByLocaleSuccess = (
	state: IShopState,
	action: IActions.IGetShippingCarrierByLocaleSuccess
) => {
	const shippingCarriesLength = action.payload.shipping_carriers.length;

	return {
		...state,
		isLoadingShippingCarrier: false,
		shippingCarriers: action.payload.shipping_carriers,
		shippingCarriesLength,
	};
};
const onGetShippingCarrierByLocaleFail = (
	state: IShopState,
	action: IActions.IGetShippingCarrierByLocaleFail
) => {
	return {
		...state,
		isLoadingShippingCarrier: false,
	};
};

const onGetRefreshTokenUrl = (state: IShopState, action: IActions.IGetRefreshTokenUrl) => {
	return {
		...state,
		isRefreshingToken: true,
	};
};
const onGetRefreshTokenUrlSuccess = (
	state: IShopState,
	action: IActions.IGetRefreshTokenUrlSuccess
) => {
	return {
		...state,
		isRefreshingToken: false,
	};
};
const onGetRefreshTokenUrlFail = (state: IShopState, action: IActions.IGetRefreshTokenUrlFail) => {
	return {
		...state,
		isRefreshingToken: false,
	};
};

const onGetOriginalProductImage = (
	state: IShopState,
	action: IActions.IGetOriginalProductImage
) => {
	return {
		...state,
		isGettingProductImageUrl: true,
	};
};
const onGetOriginalProductImageSuccess = (
	state: IShopState,
	action: IActions.IGetOriginalProductImageSuccess
) => {
	const { image_url } = action.payload;
	const currentOrder = state.currentOrder;
	downloadImage(image_url, currentOrder?.product_name as string);
	return {
		...state,
		isGettingProductImageUrl: false,
		currentOrder: undefined,
	};
};
const onGetOriginalProductImageFail = (
	state: IShopState,
	action: IActions.IGetOriginalProductImageFail
) => {
	return {
		...state,
		isGettingProductImageUrl: false,
		currentOrder: undefined,
	};
};
