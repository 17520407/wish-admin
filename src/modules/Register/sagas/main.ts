import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';
import { push } from 'connected-react-router';

// Handle User Register
function* handleUserRegister(action: any) {
	try {
		const res = yield call(AuthApi.register, action.payload);
		yield delay(200);
		console.log('res', res);
		if (res.status === 200 || res.status === 201) {
			message.success('Register successful', 2);
			yield put(actions.userRegisterSuccess(res));
			yield put(push('/login'));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			throw new Error(details[0].message);
		}
	} catch (error) {
		yield put(actions.userRegisterFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserRegister() {
	yield takeEvery(Keys.USER_REGISTER, handleUserRegister);
}
/*-----------------------------------------------------------------*/
const sagas = [watchUserRegister];

export default sagas;
