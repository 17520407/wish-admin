import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { IRegisterProps } from '../model/IRegisterProps';
import { RegisterForm } from './Form';
import { Row, Col, Divider } from 'antd';

import './index.scss';

interface IProps extends RouteComponentProps, IRegisterProps {}

export const RegisterPage: React.FC<IProps> = (props) => {
	return (
		<Row align="middle" className="auth_container">
			<Col span={8} offset={8} className="auth__content">
				<div className="text-center mb-1">
					{/* <span className="brand">Logo</span> */}
					<h5 className="mb-1 mt-3">Register</h5>
					<p className="text-muted">Create new an account</p>
				</div>
				<div className="auth-form">
					<RegisterForm {...props} />
					<Divider />
					<p className="text-muted text-center">
						Already have an account?{' '}
						<Link to="/login" className="text-primary">
							Log In
						</Link>
					</p>
				</div>
			</Col>
		</Row>
	);
};
