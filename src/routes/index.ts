import React, { ComponentClass, FunctionComponent, LazyExoticComponent } from 'react';
import { GetCodePage } from '../components';
import { routeName } from './routes-name';

const LoginPage = React.lazy(() => import('../modules/Login/components/LoginPageContainer'));
const ResetPasswordPage = React.lazy(
	() => import('../modules/ResetPassword/components/ResetPasswordPageContainer')
);
const RegisterPage = React.lazy(
	() => import('../modules/Register/components/RegisterPageContainer')
);
const UserPage = React.lazy(() => import('../modules/User/components/UserPageContainer'));
const ShopPage = React.lazy(() => import('../modules/Shop/components/ShopPageContainer'));

export interface RouteConfig {
	path: string;
	extract: boolean;
	component: ComponentClass | FunctionComponent | LazyExoticComponent<any>;
	permission?: string;
}

export const authRoutes: RouteConfig[] = [
	{
		path: routeName.login,
		extract: true,
		component: LoginPage,
	},
	{
		path: routeName.register,
		extract: true,
		component: RegisterPage,
	},
	{
		path: routeName.resetPassword,
		extract: true,
		component: ResetPasswordPage,
	},
];

export const mainRoutes: RouteConfig[] = [
	{
		path: routeName.shop,
		extract: true,
		component: ShopPage,
	},
	{
		path: routeName.user,
		extract: true,
		component: UserPage,
		permission: 'Admin',
	},
	{
		path: routeName.shopCode,
		extract: true,
		component: GetCodePage,
	},
];
